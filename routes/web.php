<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'frontend\HomeController@index')->name('homepage');
Route::post('/newsletter', 'frontend\HomeController@store')->name('homepage.submit');


Route::get('/contacts', 'frontend\ContactsController@index')->name('contacts');
Route::post('/contacts', 'frontend\ContactsController@contactus')->name('contacts.submit');
Route::post('/adhesion', 'frontend\ContactsController@adhesion')->name('adhesion.submit');


/* --- backoffice Routes --- */

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {
        Route::prefix('admin')->group( function(){
            
            /* ---  Login  --- */
            Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
            Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

            /* --- Reset Password --- */
            // Password reset routes
            Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
            Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
            Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
            Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

            /* --- Logout --- */
            Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

            /* --- Dashboard --- */
            Route::get('/', 'backoffice\DashboardController@index')->name('admin.dashboard');
            
            /* --- News --- */
            Route::get('/news', 'backoffice\NewsController@index')->name('admin.news');
            Route::get('/news/create','backoffice\NewsController@create')->name('admin.news.create');
            Route::post('/news/create', 'backoffice\NewsController@store')->name('admin.news.create.submit');
            Route::get('/news/edit/{id}', 'backoffice\NewsController@edit')->name('admin.news.edit');
            Route::post('/admin/news/edit', 'backoffice\NewsController@update')->name('admin.news.edit.submit');
            Route::get('/news/duplicate/{id}', 'backoffice\NewsController@duplicate')->name('admin.news.duplicate');
            Route::get('/news/delete/{id}', 'backoffice\NewsController@destroy')->name('admin.news.delete');

            /* --- Reviews --- */
            Route::get('/reviews', 'backoffice\ReviewsController@index')->name('admin.reviews');
            Route::get('/reviews/create','backoffice\ReviewsController@create')->name('admin.reviews.create');
            Route::post('/reviews/create', 'backoffice\ReviewsController@store')->name('admin.reviews.create.submit');
            Route::get('/reviews/edit/{id}', 'backoffice\ReviewsController@edit')->name('admin.reviews.edit');
            Route::post('/reviews', 'backoffice\ReviewsController@update')->name('admin.reviews.edit.submit');
            Route::get('/reviews/delete/{id}', 'backoffice\ReviewsController@destroy')->name('admin.reviews.delete');

            /* --- Contacts --- */
            Route::get('/contacts', 'backoffice\ContactsController@index')->name('admin.contacts');
            Route::get('/contacts/edit/{id}', 'backoffice\ContactsController@edit')->name('admin.contacts.edit');
            Route::post('/contacts', 'backoffice\ContactsController@update')->name('admin.contacts.edit.submit');

            /* --- Users --- */
            Route::get('/users', 'backoffice\UsersController@index')->name('admin.users');
            Route::get('/users/edit/{id}/{userType}', 'backoffice\UsersController@edit')->name('admin.users.edit');
            Route::post('/users', 'backoffice\UsersController@update')->name('admin.users.edit.submit');
            Route::get('/users/create','backoffice\UsersController@create')->name('admin.users.create');
            Route::post('/users/create', 'backoffice\UsersController@store')->name('admin.users.create.submit');
            Route::get('/users/delete/{id}/{userType}', 'backoffice\UsersController@destroy')->name('admin.users.delete');

            /* --- Edit Current User Profile --- */
            Route::get('/users/edit/current/{id}/{userType}', 'backoffice\UsersController@edit')->name('admin.user.edit');

            /* --- Newsletter --- */
            Route::get('/newsletter', 'backoffice\NewsletterController@index')->name('admin.newsletter');
            Route::get('/newsletter/delete/{id}', 'backoffice\NewsletterController@destroy')->name('admin.newsletter.delete');
            Route::get('/newsletter/download', 'backoffice\NewsletterController@download')->name('admin.newsletter.download.submit');
        });
    });
    

/* AUTH */ 

Auth::routes();
