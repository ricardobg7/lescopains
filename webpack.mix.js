let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/*
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
*/

/* ___ BACKOFFICE SCRIPTS __ */

mix.sass('resources/assets/sass/backoffice/style.scss', 'public/backoffice/css/style.css', {
    outputStyle: 'compressed'
  })
mix.sass('resources/assets/sass/app.scss', 'public/css/app.css');


mix.scripts([
    'resources/assets/js/backoffice/default.js',
    'resources/assets/js/backoffice/news.js',
], 'public/backoffice/js/scripts.js');


/* ___ FRONTEND SCRIPTS __ */

mix.sass('resources/assets/sass/frontend/style.scss', 'public/frontend/css/style.css', {
    outputStyle: 'compressed'
  })
  
  mix.scripts([
    'resources/assets/js/frontend/script.js',
  ], 'public/frontend/js/scripts.js');
  
  