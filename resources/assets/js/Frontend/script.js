/* FACEBOOK SHARE */
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  

  /* HEADER */
  jQuery(document).scroll(function() {
    if(jQuery(window).scrollTop() > 100){
        jQuery('.desktop-menu').css('display', 'block');
    }else{
        jQuery('.desktop-menu').css('display', 'none');

    }
});

//SLIDER
jQuery('document').ready(function(){
    jQuery('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:3
            }
        }
    });
});

/* MODAL */
jQuery('.modalBtn').on('click',function(e){
    var target = jQuery(e.currentTarget).attr('data'); 
    
    jQuery('.modal-body'+target).load('content.html',function(){
        jQuery('#myModal'+target).modal({show:true});
    });
});




