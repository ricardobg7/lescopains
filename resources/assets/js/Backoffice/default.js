jQuery(document).ready(function(e) {  
  //Button click to toggle
    jQuery( ".sidebar-toggle" ).click(function() {

      // Show/Hide Side Bar
        jQuery('.main-sidebar').animate({width: 'toggle'});
      // Show/Hide Main Content
      // Add class isOut for Show/Hide
        jQuery('.main-content').toggleClass('isOut');
        var isOut = jQuery('.main-content').hasClass('isOut');
        jQuery('.main-content').animate({marginLeft: isOut ? '0px' : '230px'}, 300);
    });

  
  // Generate Thumbnail for input files (images)
    jQuery("#thumbnail-en").change(function(){
      readURLEN(this);
    });

    jQuery("#thumbnail-fr").change(function(){
      readURLFR(this);
    });

    function readURLEN(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                jQuery('#preview-en').attr('src', e.target.result);
                jQuery('#preview-en').css('display', 'block');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    function readURLFR(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              jQuery('#preview-fr').attr('src', e.target.result);
              jQuery('#preview-fr').css('display', 'block');
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  //Tabs
    jQuery( function() {
      jQuery('#tabs').tabs();
    });

    jQuery("#submit").disabled = false; 

    //Rich text editor
    tinymce.init({
      selector: "textarea",
      theme: "modern",
      plugins: [
           "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
           "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
           "save table contextmenu directionality emoticons template paste textcolor image code"
     ],
     content_css: "css/content.css",
     toolbar: "insertfile undo redo | link image | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
     style_formats: [
          {title: 'Bold text', inline: 'b'},
          {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
          {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
          {title: 'Example 1', inline: 'span', classes: 'example1'},
          {title: 'Example 2', inline: 'span', classes: 'example2'},
          {title: 'Table styles'},
          {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
      ],

      // enable title field in the Image dialog
    image_title: true, 
    // enable automatic uploads of images represented by blob or data URIs
    automatic_uploads: true,
    // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
    // images_upload_url: 'postAcceptor.php',
    // here we add custom filepicker only to Image dialog
    file_picker_types: 'image', 
    // and here's our custom image picker
    file_picker_callback: function(cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      
      // Note: In modern browsers input[type="file"] is functional without 
      // even adding it to the DOM, but that might not be the case in some older
      // or quirky browsers like IE, so you might want to add it to the DOM
      // just in case, and visually hide it. And do not forget do remove it
      // once you do not need it anymore.
  
      input.onchange = function() {
        var file = this.files[0];
        
        var reader = new FileReader();
        reader.onload = function () {
          // Note: Now we need to register the blob in TinyMCEs image blob
          // registry. In the next release this part hopefully won't be
          // necessary, as we are looking to handle it internally.
          var id = 'blobid' + (new Date()).getTime();
          var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);
  
          // call the callback and populate the Title field with the file name
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      };
      
      input.click();
    }
  }); 







jQuery("#submit").disabled = false; 

});


  //Sort table
  window.setTimeout("HideAlertDiv()", 2000); //Hide Div after 2 seconds

  function HideAlertDiv()
  {
     jQuery('.alert').remove();
  }


