  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>

<div class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="logo-container col-md-2 offset-md-1">
                <a href="/" class="logo">
                    <img src={{ asset('/frontend/images/logo.png') }}>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="header-card  col-md-4 offset-md-1">
                    <h2><strong>L´aide aux enfants<br>
                    des centres<br>
                    d´acceuil du<br>
                    Portugal
                    </strong></h2>
            </div>

            <div class="fb-share offset-md-5 col-md-1" data-href="http://comup-dev.pt/dev/lescopainsdhugo/public/" data-layout="button" data-size="small" data-mobile-iframe="true">
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fcomup-dev.pt%2Fdev%2Flescopainsdhugo%2Fpublic%2F&amp;src=sdkpreparse" class="btn share fb-xfbml-parse-ignore">
                    <i class="sharebtn large far fa-share-square"></i>
                </a>
            </div>
            
           
        </div>
    </div>
</div>


