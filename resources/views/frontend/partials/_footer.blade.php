<div class="main-footer">
    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6  contacts-form">
                        <h3> CONTACT US  </h3>
                        <form class="needs-validation" enctype='multipart/form-data'  role="form" method="post" action="{{ action('frontend\ContactsController@contactus') }}" novalidate>
                            {{ csrf_field() }}  
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Name</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="name" placeholder="Name"  required>
                                    <div class="invalid-feedback">
                                        Please insert a valid name.
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom02">Email</label>
                                    <input type="email" class="form-control" id="validationCustom02" name="email" placeholder="Email" required>
                                    <div class="invalid-feedback">
                                        Please insert a valid email.
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom03">Message</label>
                                    <textarea class="form-control" rows="5" id="validationCustom03" name="message" required></textarea>
                                    <div class="invalid-feedback">
                                        Please provide a valid city.
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>
                </div>
                <div class="col-md-6  subscribe-form">
                        <h3>BULLETIN D'ADHESION</h3>
                        <form class="needs-validation" enctype='multipart/form-data'  role="form" method="post" action="{{ action('frontend\ContactsController@adhesion') }}" novalidate>
                            {{ csrf_field() }}  
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="inlineRadio1">Je soussigné(e):</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="male">
                                        <label class="form-check-label" for="inlineRadio1">M.</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="female">
                                        <label class="form-check-label" for="inlineRadio2">Mnme</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="neither">
                                        <label class="form-check-label" for="inlineRadio3">Sté</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">First name</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="First name" name="firstname" required>
                                    <div class="invalid-feedback">
                                        Please insert a valid first name
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom02">Last name</label>
                                    <input type="text" class="form-control" id="validationCustom02" placeholder="Last name" name="lastname" required>
                                    <div class="invalid-feedback">
                                        Please insert a valid last name
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustomUsername">Adress</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustomUsername" name="address" aria-describedby="inputGroupPrepend" required>
                                        <div class="invalid-feedback">
                                            Please insert a valid address
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustomUsername">Secondary Adress</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustomUsername" name="secondaryAddress" aria-describedby="inputGroupPrepend" required>
                                        <div class="invalid-feedback">
                                            Please insert a valid secondary address
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom02">Ville</label>
                                    <input type="text" class="form-control" id="validationCustom02" name="ville" required>
                                    <div class="invalid-feedback">
                                        Please insert a valid ville
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Postal code</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="postalCode"  required>
                                    <div class="invalid-feedback">
                                        Please insert a valid postal code
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Email</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="email" required>
                                    <div class="invalid-feedback">
                                        Please insert a valid email
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom02">Telephone</label>
                                    <input type="text" class="form-control" id="validationCustom02" name="phone" required>
                                    <div class="invalid-feedback">
                                        Please insert a valid phone
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                désire adhérer à l'Association "Les Copains d'Hugo" en tant que :
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <div class="form-check">
                                            <input class="form-check-input checkbox-adhesion" type="checkbox" name="adhesion" value="adhesion" id="invalidCheck" required>
                                            <label class="form-check-label" for="invalidCheck">
                                                Adhérent 25€ : cotisation annuelle
                                            </label>
                                            <div class="invalid-feedback">
                                                You must agree before submitting.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>
                    </div>
                <div class="col-md-2 offset-md-2 contacts-logo">
                    <h3> CONTACTS </h3>
                    <div class="footer-logo">
                        <img src={{ asset('/frontend/images/logo_cor.png') }}>
                    </div>
                </div>
                <div class="col-md-4 contacts">
                    <div class="contacts-footer-map">
                        <i class="fa fa-map-marker-alt"></i> 
                        <span class="contacts-footer-label">
                            Maison des Associations 14,<br>Place du Clos de Pacy<br>94370 Sucy-en-Brie 
                        </span>
                    </div>
                    <div class="contacts-footer-email">
                        <i class="fa fa-envelope"></i> 
                        <span class="contacts-footer-label">
                            {{ $contacts[0]->email }} 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row partners">
                <div class="col-md-4 offset-md-2 partners_title">
                    <h3>
                        NOS PARTENAIRES
                    </h3>
                </div>
                <div class="col-md-6"> </div>

                <div class="col-md-2"></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-2"></div>

                <div class="col-md-2"></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-1"><img src={{ asset('/frontend/images/partners/AMG.jpg') }} /></div>
                <div class="col-md-2"></div>

            </div>

            <div class="row">
                <div class="copyright">
                    <div class="copyright-label">
                        <span> © 2018 Les Copains d'Hugo</span> <br>
                    </div>
                    <div class="copyright-company">
                        <span> Tous droits réservés - Developed by ComUp </span>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>

<style>
    .partners-row > div{

    }
    </style>

<script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
          'use strict';
          window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
        </script>