<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119194217-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-119194217-1');
        </script>

        

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta property="og:url"                content="http://comup-dev.pt/dev/lescopainsdhugo/public/" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Les Copains d'Hugo" />
    <meta property="og:description"        content="Description here" />
    <meta property="og:image"              content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" />


    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap_4.1.0.min.css') }}">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('css/owl/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/owl/owl.theme.default.min.css') }}">

    <script src="{{ asset('js/jquery.min.js') }}" ></script>
    <script src="{{ asset('js/toastr/toastr.min.js') }}" ></script>

    <script src="{{ asset('frontend/js/scripts.js') }}" defer></script>
    <script src="{{ asset('js/fontawesome.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}" defer></script>

    

    <script src="{{ asset('js/owl/owl.carousel.min.js') }}" ></script>    


    <title> Les Coupins d'Hugo </title>
    <link rel="shortcut icon" href={{ asset('/images/brand/favicon.png') }} type="image/x-icon"/>
</head> 

