
<div class="desktop-menu" style="display:none;">
    <div class="navbar">
        <nav id="navbar" class="navbar navbarTop fixed-top sticky hidden">
            <div>
                <span class="brand-span">
                    <a href="#top" class="brand-logo"><img  src={{ asset('frontend/images/logo.png')}} alt=""></a>
                </span>
            </div>
            <div class="menu-options">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a href="#top">Maison</a>
                    </li>
                    <li class="nav-item">
                        <a href="#objectivos">Nous Objectifs</a>
                    </li>
                    <li class="nav-item">
                        <a href="#presidente">Président</a>
                    </li>
                    <li class="nav-item">
                        <a href="#trabalho">Notre Travail</a>
                    </li>
                    <li class="nav-item">
                        <a href="#reviews">La Revue</a>
                    </li>
                    <li class="nav-item">
                        <a href="#contactos">Contacts</a>
                    </li>
                    <li class="nav-item">
                        <a href="#parceiros">Nos Partenaires</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
