@include('frontend.partials._head')
@include('frontend.partials._main-header')

<body>
    
    <div class="container-fluid">
        @include('frontend.partials._menu')
        
        @yield('content')
    </div>
</body>

@include('frontend.partials._footer')
<html>
