@include('Login._head')

<body>
    <div class="container loginBackground">
        @yield('content')
    </div>
</body>
<html>

<style>
.loginBackground {
    background: url('https://images.unsplash.com/photo-1461696114087-397271a7aedc') no-repeat center center fixed; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    width:100%;
    height:100%;
    margin:0;
    
}

/* LIST OF IMAGES
buildings: 
- https://image.freepik.com/free-photo/observation-urban-building-business-steel_1127-2397.jpg
- https://image.freepik.com/free-photo/modern-glass-buildings_21730-6937.jpg

tech/laptop: 
- https://image.freepik.com/free-photo/close-up-of-male-hands-using-laptop-in-home_1150-790.jpg
- 

landscape: 
- https://images.unsplash.com/photo-1461696114087-397271a7aedc

*/
</style>

