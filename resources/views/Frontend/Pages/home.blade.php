@extends('frontend.main')

@section('content')
    <div class="row objectives">
        <div class="col-md-4 offset-md-5 objectives-logo">
            <img src={{ asset('/frontend/images/logo_cor.png') }}>
        </div>
        <div class="col-md-4 offset-md-1 objectives-col1">
            <h3>
                Les Objectifs
            </h3>
            <span>
                    LES COPAINS D'HUGO a pour but d'aider les enfants qui vivent dans les centres d'accueil au Portugal. <br>
                    L'Association dispose de 2 relais : Un dans la ville de Porto Nord du Portugal et l'autre à Leiria région centre du Portugal.
            </span>
        </div>
        <div class="col-md-4 offset-md-2 objectives-col2">
            <h3>
                    Notre Mission
            </h3>
            <span>
                    Aider les Centres d'accueil pour enfants défavorisés du Portugal qui auraient besoin de contributions extérieures.<br>
                    Non pas sous forme d'argent, mais par la réalisation de projets, ou apport de matériel pour améliorer leur vie.
            </span>
        </div>
    </div>
    
    <div class="row background1">
    </div>

    <div class="row president">
        <div class="offset-md-1 col-md-11 president_title">
                <h3>LE MOT DU <br> PRÉSIDENT</h3>
        </div>
        <div class="offset-md-1 col-md-4 president_col1">
            <span>
                Il y a des expressions que l'on utilise régulièrement dans la vie courante de manière anodine,
                «il faut le voir pour le croire», «il faut le vivre pour le comprendre», «le ciel m'est tombé sur la tête»,
                et un jour on comprend mieux leur sens.
            </span>
        </div>
        <div class="offset-md-2 col-md-4 president_col1">
            <span>
                Ce jour où nous avons appris que nous ne pouvions pas avoir d'enfant...
                sans raisons et sans explications. C'est à ce moment là qu'on prend conscience
                que notre vie ne sera plus jamais la même.<br><br>
                D'abord l'incompréhension, l'injustice, la souffrance, la tristesse puis la réflexion,
                décision et enfin, comme une évidence l'adoption...l'espoir. Attente, désillusions et bureaucratie...
                tous ces mots résument notre vie pendant les 4 ans de notre parcours, notre «grossesse à risque» pour
                devenir parents.<br>
            </span>
        </div>
    </div>

    <div class="row background2">
    </div>

    <div class="row events">
            <div class="col-md-4 offset-md-2 travel_col">
                    <h3>NOTRE TRAVAIL</h3>
                    <span>Nous souhaitons grâce à notre association renforcer et faciliter l'échange entre le
                        monde Portugais et Français, promouvoir, faciliter
                        l'ouverture dans les échanges et la culture.<br>
    
                        Cela nous conduira à l'organisation d'évènements Sociaux-Culturels
                        Franco-Portugais qui permettront de récolter les fonds nécessaires
                        pour donner un peu de bonheur.<br>
                    </span>
            </div>
            <div class="col-md-10 offset-md-2 events_col">
                <h3>Nos Évènements</h3>
                <!-- news slider -->
            </div>
            <div class="col-md-2">
            </div>
                @foreach($news as $event)

                    <div class="card-wrapper col-md-2">
                        <div id="card-2" class="card-rotating effect__click h-100 w-100">
                            <!--Front Side-->
                            <div class="face front card">
                            <!-- Image-->
                                <div class="view overlay">
                                    <img class="card-img-top"src={{ asset( '/images/news/'.$event->id.'/'.$event->thumbnail ) }} alt="Example photo">
                                    <a>
                                    <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            <!--Content-->
                                <div class="card-body">
                                    <a class="rotate-btn float-right" data-card="card-2"></a>
                                    <h4 class=""> {{ $event->title }}  </h4>
                                    <hr>
                                    <p class="card-text">{{ $event->subtitle }} </p>
                                    <button type="button" class="btn btn-primary modalBtn" data-toggle="modal" data-target="myModal{{ $event->id }}" data="{{$event->id}}">
                                            See More
                                    </button>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-lg" id="myModal{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" id="modalDialog{{$event->id}}" >
                                        <div class="modal-content" >
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">{{ $event->title }}</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body{{ $event->id }} text-center pagination-centered">
                                            <span> {!! $event->description !!} </span> <br>
                                            <img src={{ asset( '/images/news/'.$event->id.'/'.$event->thumbnail ) }} alt="Example photo">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-2">
                </div>
    </div>

    <div class="row background3">
    </div>

    <div class="row reviews">
        <div class="col-md-10 offset-md-2">
            <h3>NOS REVIEWS</h3>
        </div>

        <div class="col-md-2">
        </div>

            <div class="owl-carousel owl-theme col-md-8">
                @foreach($reviews as $review)
                    <div class="item">
                        <figure class="snip1192">
                            <blockquote> {{$review->quote}} </blockquote>
                            <div class="author">
                                <img src="images/reviews/{{$review->id}}/{{ $review->thumbnail}}"  alt="sq-sample1"/>
                                <h5> {{ $review->name}} <span> {{ $review->company}} </span></h5>
                            </div>
                        </figure>
                    </div>
                @endforeach
            </div>


        <div class="col-md-2">
        </div>
    </div>

@endsection