<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118576739-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118576739-1');
</script>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('Backoffice/css/style.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/jquery/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap_4.1.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap_3.3.7.min.css') }}">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css">


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/toastr/toastr.min.js') }}" ></script>
    <script src="{{ asset('Backoffice/js/scripts.js') }}"></script>
    <script src="{{ asset('js/fontawesome.js') }}"></script>
    <script src="{{ asset('js/validator.min.js') }}"></script>
    <script src="{{ asset('js/jquery/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery/jquery.dataTables.min.js') }}"></script>





    <title> Comup </title>
</head> 
