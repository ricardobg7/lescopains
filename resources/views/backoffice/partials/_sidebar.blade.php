<?php $currentUser = Auth::user(); ?>

<div class="main-sidebar">
    <ul class="sidebar-menu">
        <div class="user-panel">
            <div class="image">
                <img src={{ asset('/images/user/user-logo.jpg') }} class="img-circle" alt="User Image">
            </div>
            <div class="user-name">
                <span>{{ __('backoffice/global.logged') }}<b> <?php echo $currentUser['attributes']['firstName']; ?> <?php echo $currentUser['attributes']['lastName']; ?></b></span>
                <br>
                <a class="text-warning" href="{{ route('admin.user.edit', ['id' => $currentUser['attributes']['id'], 'userType' => $currentUser['attributes']['type']])}}"> {{ __('backoffice/global.edit-profile') }} </a> 
            </div>
        </div>

        <li class="header nohover">{{ __('backoffice/global.menu') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li {{{ (Request::is('admin') ? 'class=active' : '') }}} >
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i> 
                    <span>{{ __('backoffice/global.dashboard') }}</span>
                </a>
            </li>

            <li {{{ (Request::is('admin/news') ? 'class=active' : '') }}} >
                <a href="{{ route('admin.news') }}">
                    <i class="fa fa-newspaper"></i> 
                    <span>{{ __('backoffice/global.news') }}</span> 
                </a>
            </li>

            <li {{{ (Request::is('admin/reviews') ? 'class=active' : '') }}} >
                <a href="{{ route('admin.reviews') }}">
                    <i class="fa fa-quote-left"></i> 
                    <span>{{ __('backoffice/global.reviews') }}</span> 
                </a>
            </li>
            @if($currentUser['attributes']['type'] < 3 )
                <li {{{ (Request::is('admin/users') ? 'class=active' : '') }}} >
                    <a href="{{ route('admin.users') }}">
                        <i class="fa fa-users"></i> 
                        <span>{{ __('backoffice/global.users') }}</span>
                    </a>
                </li>
            @endif

            <li {{{ (Request::is('admin/contacts') ? 'class=active' : '') }}} >
                <a href="{{ route('admin.contacts') }}">
                    <i class="fa fa-users"></i> 
                    <span>{{ __('backoffice/global.contacts') }}</span>
                </a>
            </li>
            
            <li {{{ (Request::is('admin/newsletter') ? 'class=active' : '') }}} >
                <a href="{{ route('admin.newsletter') }}">
                    <i class="fa fa-newspaper"></i> 
                    <span>{{ __('backoffice/global.newsletter') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.logout') }}">
                    <i class="fa fa-sign-out-alt"></i> 
                    <span>{{ __('backoffice/global.logout') }}</span>
                </a>
            </li>
                        
    </ul>

</div>
