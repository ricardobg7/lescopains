
<header class="main-header">

    <a href="/admin/" class="logo">
        <td><img src={{ asset('/images/brand/brand-logo.png') }}></td>    </a>
    
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <i class="fa fa-list"> </i>
        </a>

        <ul class="languageSelector">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li class="languageSelector-items">
                        <a class="text-warning" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            @if( $properties['name'] == 'English')
                                <div class="languages-selector">
                                    <div class="language-icon">
                                        <img src={{ asset('/images/languages/us.jpg') }}>
                                    </div>
                                </div>
                            @else
                                <div class="languages-selector">
                                    <div class="language-icon">
                                        <img src={{ asset('/images/languages/fr.jpg') }}>
                                    </div>
                                </div>
                            @endif
                        </a>
                    </li>
                @endforeach
        </ul>

    </nav>


</header>