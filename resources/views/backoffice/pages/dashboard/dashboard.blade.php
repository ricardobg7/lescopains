@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">

        <section class="content-header">
            <h1>{{ __('backoffice/global.dashboard') }} <small>{{ __('backoffice/global.dashboard-description') }}</small></h1>
        </section>

        <section class="container-fluid box">
            <div class="box-body">

                    <div class="row">
                            <div class="col-md-4 card">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div class="media-body">
                                            {{$users}}
                                            <p> {{ __('backoffice/dashboard.users') }}  <p>
                                        </div>         
                                    </div>
                                </div>
            
                                <div class="col-md-4 card">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-eye"></i>
                                        </div>
                                        <div class="media-body">
                                            {{$views}}
                                            <p> {{ __('backoffice/dashboard.views') }}   <p>
                                        </div>         
                                    </div>
                                </div>
            
                                <div class="col-md-4 card">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-clock"></i>
                                        </div>
                                        <div class="media-body">
                                            {{$time}}
                                            <p> {{ __('backoffice/dashboard.time') }}   <p>
                                        </div>         
                                    </div>
                                </div>
                    </div>
                    <div class="row">
                                <div class="col-md-12 listViews">
                                        <div id="map-div"></div>
                                        {!! \Lava::render('GeoChart', 'map', 'map-div') !!}
                                </div>
                    </div>

                    <div class="row">
                                <div class="col-md-6 listViews" >
                                        <h3 class="text-center"> {{ __('backoffice/dashboard.most-viewed') }}  </h3>
                                        <ul class="list-group ">
                                            @foreach($trending as $page)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                {{ $page['url'] }}
                                            
                                                <span class="badge-primary badge-pill">{{ $page['pageViews'] }}</span>
                                            </li>
                                            @endforeach
                                        </ul>
                                </div>
                                <div class=" col-md-4 offset-md-1 listViews fit" >
                                        <h3 class="text-center"> {{ __('backoffice/dashboard.most-used-device') }}  </h3>
                                        <div id="pie-div" ></div>
                                        {!! \Lava::render('PieChart', 'devicesPie', 'pie-div') !!}
            
                                </div>
                            </div>
                    </div>

            </div>
        </section>
        
    </div>
    </div>

</div>


@endsection
