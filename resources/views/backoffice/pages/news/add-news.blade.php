@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">
        <section class="box">
            <div class="box-body">
                    <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\NewsController@store') }}" id="formExemplo" data-toggle="validator" role="form">
                        {{ csrf_field() }}  
                                    <p class="title-form"> New Event </p>
                                    <div class="form-group">	    
                                        <label for="textTitle" class="control-label">{{ __('backoffice/form.title') }}</label>	    
                                        <input name="textTitle" id="textTitle" class="form-control" placeholder="{{ __('backoffice/form.title') }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                                        <div class="help-block with-errors">
                                        </div>
                                    </div>	  	  
                                    <div class="form-group">	    
                                        <label for="textSubtitle" class="control-label">{{ __('backoffice/form.subtitle') }}</label>	    
                                        <input name="textSubtitle" id="textSubtitle" class="form-control" placeholder="{{ __('backoffice/form.subtitle') }}" type="text" data-error="{{ __('backoffice/form.subtitle-error') }}" required>	    
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div>	 
                                    <div class="form-group">	    
                                        <label for="textDescription" class="control-label">{{ __('backoffice/form.description') }}</label>	    
                                        <textarea id="elm1" name="textDescription" id="textDescription" class="form-control" data-error="{{ __('backoffice/form.description-error') }}" required></textarea>
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div> 	  
                                    <div class="form-group">	    
                                        <label for="thumbnail" class="control-label">{{ __('backoffice/form.image') }}</label>	    
                                        <input name="thumbnail[]" id="thumbnail" class="form-control" placeholder="{{ __('backoffice/form.image') }}" type="file" data-error="{{ __('backoffice/form.image-error') }}" accept="image/*" required>	    
                                        <img id="preview" src="#" style="display:none;"/>
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div>
                                    

                                    <div class="form-group submit-form">
                                        <button type="submit" role="button" id="submit" class="btn btn-primary">{{ __("backoffice/form.submit-button") }}</button>                                        
                                    </div>
                    </form>
            </div>
        </section>
        
    </div>
    </div>
</div>

@endsection
