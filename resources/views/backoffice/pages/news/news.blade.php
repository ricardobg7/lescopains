@extends('backoffice.main')

@section('content')

<?php  $locale = App::getLocale();?>

<div class="main-content">
    <div class="col-md-12">

        <section class="content-header">
            <h1>{{ __('backoffice/global.news') }}</h1>
        </section>

        <a href="{{ route('admin.news.create') }}"> <button type="button" class="btn btn-success" >{{ __('backoffice/news.actions-add') }}</button> </a>

        <section class="box">
            <div class="box-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('backoffice/news.title') }}</th>
                            <th scope="col">{{ __('backoffice/news.subtitle') }}</th>
                            <th scope="col">{{ __('backoffice/news.description') }}</th>
                            <th scope="col">{{ __('backoffice/news.thumbnail') }}</th>
                            <th scope="col">{{ __('backoffice/news.actions') }}</th>
                        </tr>
                    </thead>
                    @foreach ($news as $new)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $new->id }}</th>
                                <td>{{ $new->title }}</td>
                                <td>{{ $new->subtitle }}</td>
                                <td>
                                    <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $new->id }}">Preview</button>
                                    
                                    <div class="modal fade" id="{{ $new->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                        {!! $new->description !!}
                                                        <img src={{ asset('/images/news/' . $new->newsId . '/'. $new->thumbnail) }}>
                                                </div>
                                              </div>
                                            </div>
                                          </div>                     
                                </td>
                                <td><img class="news-thumb" src={{ asset('/images/news/' . $new->newsId . '/'. $new->thumbnail) }}></td>
                                <td>
                                    <a href="{{ route('admin.news.edit', ['id' => $new->newsId ])}}"> <i class="fa fa-edit icon-edit"></i> </a> 
                                    <a href="{{ route('admin.news.delete', ['id' => $new->newsId ])}}" onclick="return confirm('{{ __('backoffice/form.news-alert-remove') }}')"> <i class="fa fa-trash-alt icon-trash-alt"></i> </a> 
                                    <a href="{{ route('admin.news.duplicate', ['id' => $new->newsId ])}}" onclick="return confirm('{{ __('backoffice/form.news-alert-duplicate') }}')"> <i class="fa fa-copy icon-copy"></i> </a> 
                                </td>
                            </tr>
                        </tbody>
                    @endforeach
                        <tfoot>
                            <tr class="thead-dark">
                                <th scope="col">#</th>
                                <th scope="col">{{ __('backoffice/news.title') }}</th>
                                <th scope="col">{{ __('backoffice/news.subtitle') }}</th>
                                <th scope="col">{{ __('backoffice/news.description') }}</th>
                                <th scope="col">{{ __('backoffice/news.thumbnail') }}</th>
                                <th scope="col">{{ __('backoffice/news.actions') }}</th>
                            </tr>
                        </tfoot>
                </table>

            </div>
        </section>
        
    </div>
    </div>

</div>

{!! Toastr::message() !!}


@endsection