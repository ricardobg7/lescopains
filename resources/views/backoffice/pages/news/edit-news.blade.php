@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">
        <section class="box">
            <div class="box-body">
                
                    <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\NewsController@update') }}" id="formExemplo" data-toggle="validator" role="form">
                        {{ csrf_field() }}  

                                    <p class="title-form"> Edit {{ $newsTranslations->title}} </p>
                                    <div class="form-group">	    
                                        <label for="textTitle" class="control-label">{{ __('backoffice/form.title') }}</label>	    
                                        <input name="textTitle" id="textTitle" class="form-control" value="{{ $newsTranslations->title }}" placeholder="{{ $newsTranslations->title }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                                        <div class="help-block with-errors">
                                        </div>
                                    </div>	  	  
                                    <div class="form-group">	    
                                        <label for="textSubtitle" class="control-label">{{ __('backoffice/form.subtitle') }}</label>	    
                                        <input name="textSubtitle" id="textSubtitle" class="form-control" value="{{ $newsTranslations->subtitle }}" placeholder="{{ $newsTranslations->subtitle }}" type="text" data-error="{{ __('backoffice/form.subtitle-error') }}" required>	    
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div>	 
                                    <div class="form-group">	    
                                        <label for="textDescription" class="control-label">{{ __('backoffice/form.description') }}</label>	    
                                        <textarea name="textDescription" id="textDescription" class="form-control" placeholder="{{ $newsTranslations->description }}" data-error="{{ __('backoffice/form.description-error') }}" required> {{ $newsTranslations->description }} </textarea>
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div> 	  
                                    <div class="form-group">	    
                                        <label for="thumbnail" class="control-label">{{ __('backoffice/form.image') }}</label>	    
                                        <input name="thumbnail[]" id="thumbnail" class="form-control" placeholder="{{ __('backoffice/form.image') }}" type="file" data-error="{{ __('backoffice/form.image-error') }}" accept="image/*" >	    
                                        <?php $path = '/images/news/'. $newsTranslations->newsId.'/'.$newsTranslations->thumbnail; ?>
                                        <img class="news-thumb" src="<?php echo $path; ?>">
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div>

                                    <input type="hidden" name="id" value="{{ $news[0]->id }}">

                                    <div class="form-group submit-form">
                                            <button type="submit" role="button" id="submit" class="btn btn-primary">{{ __("backoffice/form.submit-button") }}</button>                                        </div>
                                    </div>
                    </form>
            </div>
        </section>
        
    </div>
    </div>
</div>

@endsection
