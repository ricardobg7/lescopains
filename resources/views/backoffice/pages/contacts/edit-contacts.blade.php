@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">
        <section class="box">
            <div class="box-body">
                    <form role="form" method="post" action="{{ action('backoffice\ContactsController@update') }}" id="formExemplo" data-toggle="validator" role="form">
                        {{ csrf_field() }}  
                        <div class="form-group">	    
                            <label for="textTitle" class="control-label">{{ __('backoffice/contacts.email') }}</label>	    
                            <input name="email" id="email" class="form-control" value="{{ $contacts[0]->email }}" placeholder="{{ $contacts[0]->email }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>
                        </div>	  	  
                        <div class="form-group">	    
                            <label for="textDescription" class="control-label">{{ __('backoffice/contacts.phone') }}</label>	    
                            <input name="phone" id="phone" class="form-control" value="{{ $contacts[0]->phone }}" placeholder="{{ $contacts[0]->phone }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>	  
                        </div> 	  
                        <div class="form-group">	    
                            <label for="textDescription" class="control-label">{{ __('backoffice/contacts.secondaryphone') }}</label>	    
                            <input name="secondaryPhone" id="secondaryPhone" class="form-control" value="{{ $contacts[0]->secondaryPhone }}" placeholder="{{ $contacts[0]->secondaryPhone }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>	  
                        </div> 	 
                        <div class="form-group">	    
                            <label for="textDescription" class="control-label">{{ __('backoffice/contacts.adress') }}</label>	    
                            <input name="adress" id="adress" class="form-control" value="{{ $contacts[0]->adress }}" placeholder="{{ $contacts[0]->adress }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>	  
                        </div> 	
                        <div class="form-group">	    
                            <label for="textDescription" class="control-label">{{ __('backoffice/contacts.facebook') }}</label>	    
                            <input name="facebook" id="facebook" class="form-control" value="{{ $contacts[0]->facebook }}" placeholder="{{ $contacts[0]->facebook }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>	  
                        </div> 	 
                        <div class="form-group">	    
                            <label for="textDescription" class="control-label">{{ __('backoffice/contacts.twitter') }}</label>	    
                            <input name="twitter" id="twitter" class="form-control" value="{{ $contacts[0]->twitter }}" placeholder="{{ $contacts[0]->twitter }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>	  
                        </div> 	  
                        <div class="form-group">	    
                            <label for="textDescription" class="control-label">{{ __('backoffice/contacts.linkedin') }}</label>	    
                            <input name="linkedin" id="linkedin" class="form-control" value="{{ $contacts[0]->linkedin }}" placeholder="{{ $contacts[0]->linkedin }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                            <div class="help-block with-errors">
                            </div>	  
                        </div> 	 
                        <input type="hidden" name="id" value="{{ $contacts[0]->id }}">
                        <div class="form-group submit-form">
                            <button type="submit" id="submit" class="btn btn-primary">{{ __("backoffice/form.submit-button")}}</button>
                        </div>		 
                    </form>
        </section>
        
    </div>
    </div>
</div>

@endsection
