@extends('backoffice.main')

@section('content')


<div class="main-content">
    <div class="col-md-12">

        <section class="content-header">
            <h1>{{ __('backoffice/global.contacts') }}</h1>
        </section>



        <a href="{{ route('admin.contacts.edit', ['id' => $contacts[0]->id ])}}" > <button type="button" class="btn btn-success" >{{ __('backoffice/contacts.edit') }}</button> </a>

        <section class="box">
            <div class="box-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('backoffice/contacts.email') }}</th>
                            <th scope="col">{{ __('backoffice/contacts.phone') }}</th>
                            <th scope="col">{{ __('backoffice/contacts.secondaryphone') }}</th>
                            <th scope="col">{{ __('backoffice/contacts.adress') }}</th>
                            <th scope="col">{{ __('backoffice/contacts.facebook') }}</th>
                            <th scope="col">{{ __('backoffice/contacts.twitter') }}</th>
                            <th scope="col">{{ __('backoffice/contacts.linkedin') }}</th>

                        </tr>
                    </thead>
                    @foreach ($contacts as $contact)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $contact->id }}</th>
                                <td>{{ $contact->email }}</td>
                                <td>{{ $contact->adress }}</td>
                                <td>{{ $contact->facebook }}</td>
                            </tr>
                        </tbody>
                    @endforeach
                        <tfoot>
                            <tr class="thead-dark">
                                <th scope="col">#</th>
                                <th scope="col">{{ __('backoffice/contacts.email') }}</th>
                                <th scope="col">{{ __('backoffice/contacts.phone') }}</th>
                                <th scope="col">{{ __('backoffice/contacts.secondaryphone') }}</th>
                                <th scope="col">{{ __('backoffice/contacts.adress') }}</th>
                                <th scope="col">{{ __('backoffice/contacts.facebook') }}</th>
                                <th scope="col">{{ __('backoffice/contacts.twitter') }}</th>
                                <th scope="col">{{ __('backoffice/contacts.linkedin') }}</th>
                            </tr>
                        </tfoot>
                </table>

            </div>
        </section>
        
    </div>
    </div>

</div>

{!! Toastr::message() !!}


@endsection