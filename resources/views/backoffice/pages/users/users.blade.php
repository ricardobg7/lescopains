@extends('backoffice.main')

@section('content')

<div class="main-content">
        <div class="col-md-12">
    
            <section class="content-header">
                <h1>{{ __('backoffice/global.users') }}</h1>
            </section>

        <a href="{{ route('admin.users.create') }}"> <button type="button" class="btn btn-success" >{{ __('backoffice/users.add') }}</button> </a>
  
        <section class="box">
            <div class="box-body">
                    <table id="table" class="table table-hover table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">{{ __('backoffice/users.firstname') }}</th>
                                    <th scope="col">{{ __('backoffice/users.lastname') }}</th>
                                    <th scope="col">{{ __('backoffice/users.email') }}</th>
                                    <th scope="col">{{ __('backoffice/users.type') }}</th>
                                    <th scope="col">{{ __('backoffice/users.actions') }}</th>
                                </tr>
                            </thead>

                            @foreach ($allUsers as $user)
                            
                                <tbody>
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->firstName }}</td>
                                        <td>{{ $user->lastName }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                        @php
                                        switch($user->type){
                                            case(1):
                                                echo "<b>Super Admin</b>";
                                                break;
                                            case(2):
                                                echo "<b>Admin</b>";
                                                break;
                                            case(3):
                                                echo "Publisher";
                                                break;
                                            default:
                                                echo "User";
                                                break;
                                        }
                                        @endphp
                                        </td>
                                        <td>
                                            @if($user->type == 1)
                                                @if($currentUser->type == 1)
                                                    <a href="{{ route('admin.users.edit', ['id' => $user->id, 'userType' => $user->type])}}"> <i class="fa fa-edit icon-edit"></i> </a> 
                                                    <a href="{{ route('admin.users.delete', ['id' => $user->id, 'userType' => $user->type])}}" onclick="return confirm('{{ __('backoffice/form.news-alert-remove') }}')"> <i class="fa fa-trash-alt icon-trash-alt"></i> </a> 
                                                @endif   
                                            @else
                                                <a href="{{ route('admin.users.edit', ['id' => $user->id, 'userType' => $user->type])}}"> <i class="fa fa-edit icon-edit"></i> </a> 
                                                <a href="{{ route('admin.users.delete', ['id' => $user->id, 'userType' => $user->type])}}" onclick="return confirm('{{ __('backoffice/form.news-alert-remove') }}')"> <i class="fa fa-trash-alt icon-trash-alt"></i> </a>       
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                                <tfoot>
                                    <tr class="thead-dark">
                                        <th scope="col">#</th>
                                        <th scope="col">{{ __('backoffice/users.firstname') }}</th>
                                        <th scope="col">{{ __('backoffice/users.lastname') }}</th>
                                        <th scope="col">{{ __('backoffice/users.email') }}</th>
                                        <th scope="col">{{ __('backoffice/users.type') }}</th>
                                        <th scope="col">{{ __('backoffice/users.actions') }}</th>
                                    </tr>
                                </tfoot>
                        </table>
            </div>
        </section>
        
    </div>
    </div>

</div>

{!! Toastr::message() !!}

@endsection
