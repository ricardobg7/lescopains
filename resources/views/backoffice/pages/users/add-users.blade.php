@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">
        <section class="box">
            <div class="box-body">
                <form role="form" method="post" action="{{ action('backoffice\UsersController@store') }}" id="formExemplo" data-toggle="validator" role="form">
                    {{ csrf_field() }}  
                    <div class="form-group">	    
                        <label for="textTitle" class="control-label">{{ __('backoffice/users.firstname') }}</label>	    
                        <input name="firstName" id="firstName" class="form-control" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>	  
                    <div class="form-group">	    
                        <label for="lastName" class="control-label">{{ __('backoffice/users.lastname') }}</label>	    
                        <input name="lastName" id="lastName" class="form-control" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>
                    <div class="form-group">	    
                        <label for="email" class="control-label">{{ __('backoffice/users.email') }}</label>	    
                        <input type="email" name="email" id="email" class="form-control" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                        @if(session()->has('message'))
                        <div class="alert alert-danger">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    </div>	
                    <div class="form-group">	    
                        <label for="password" class="control-label">{{ __('backoffice/users.password') }}</label>	    
                        <input type="password" name="password" id="password" class="form-control" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="control-label">{{ __('backoffice/users.type') }}</label>	    	    
                        <select class="form-control" name="type">
                            <option value="1"> Super Admin </option>
                            <option value="2"> Admin </option>
                            <option value="3"> Publisher </option>
                            <option value="4"> User </option>
                        </select>
                    </div>
                    <div class="form-group submit-form">
                        <button type="submit" id="submit" class="btn btn-primary">{{ __("backoffice/form.submit-button") }}</button>
                    </div>
                </form>
        </section>
        
    </div>
    </div>
</div>

@endsection
