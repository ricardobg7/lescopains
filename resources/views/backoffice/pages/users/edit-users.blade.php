@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">
        <section class="box">
            <div class="box-body">
                <form role="form" method="post" action="{{ action('backoffice\UsersController@update') }}" id="formExemplo" data-toggle="validator" role="form">
                    {{ csrf_field() }}  
                    <div class="form-group">	    
                        <label for="textTitle" class="control-label">{{ __('backoffice/users.firstname') }}</label>	    
                        <input name="firstName" id="firstName" class="form-control" value="{{ $user[0]->firstName }}" placeholder="{{ $user[0]->firstName }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>	  
                    <div class="form-group">	    
                        <label for="lastName" class="control-label">{{ __('backoffice/users.lastname') }}</label>	    
                        <input name="lastName" id="lastName" class="form-control" value="{{ $user[0]->lastName }}" placeholder="{{ $user[0]->lastName }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>
                    <div class="form-group">	    
                        <label for="email" class="control-label">{{ __('backoffice/users.email') }}</label>	    
                        <input type="email" name="email" id="email" class="form-control" value="{{ $user[0]->email }}" placeholder="{{ $user[0]->email }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>	
                    <div class="form-group">	    
                        <label for="password" class="control-label">{{ __('backoffice/users.password') }}</label>	    
                        <input type="password" name="password" id="password" class="form-control" value="{{ $user[0]->password }}" placeholder="{{ $user[0]->password }}" data-error="{{ __('backoffice/form.title-error') }}" type="text" required>	  
                        <div class="help-block with-errors">
                        </div>
                    </div>
                    <div class="form-group" {{ $user[0]->type == 4 ? 'style=display:none;' :  'style=display:block;' }}>
                        <label for="type" class="control-label">{{ __('backoffice/users.type') }}</label>	    	    
                        <select class="form-control" name="type">
                            @if($currentUser['attributes']['type'] == 1){ ?>
                                <option value="1"> Super Admin </option>
                            @endif
                            <option value="2"> Admin </option>
                            <option value="3"> Publisher </option>
                        </select>
                    </div>
                    <input type="hidden" name="id" value="{{ $user[0]->id }}">
                    <input type="hidden" name="oldType" value="{{ $user[0]->type }}">
                    <div class="form-group submit-form">
                        <button type="submit" id="submit" class="btn btn-primary">{{ __("backoffice/form.submit-button") }}</button>
                    </div>
                </form>
            </div>
        </section>
        
    </div>
</div>

@endsection
