@extends('backoffice.main')

@section('content')


<div class="main-content">
    <div class="col-md-12">

        <section class="content-header">
            <h1>{{ __('backoffice/global.newsletter') }}</h1>
        </section>
        
        <a href="{{ route('admin.newsletter.download.submit') }}"> <button type="button" class="btn btn-success" >{{ __('backoffice/newsletter.download') }}</button> </a>

        <section class="box">
            <div class="box-body">
                <table id="table" class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('backoffice/newsletter.email') }}</th>
                            <th scope="col">{{ __('backoffice/newsletter.actions') }}</th>
                        </tr>
                    </thead>
                    @foreach ($newsletters as $newsletter)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $newsletter->id }}</th>
                                <td>{{ $newsletter->email }}</td>
                                <td>
                                <a href="{{ route('admin.newsletter.delete', ['id' => $newsletter->id ])}}" onclick="return confirm('{{ __('backoffice/form.news-alert-remove') }}')"> <i class="fa fa-trash-alt icon-trash-alt"></i> </a> 
                                </td>
                            </tr>
                        </tbody>
                    @endforeach
                        <tfoot>
                            <tr class="thead-dark">
                            <th scope="col">#</th>
                            <th scope="col">{{ __('backoffice/newsletter.email') }}</th>
                            <th scope="col">{{ __('backoffice/newsletter.actions') }}</th>
                            </tr>
                        </tfoot>
                </table>

            </div>
        </section>
        
    </div>
    </div>

</div>


{!! Toastr::message() !!}

@endsection