@extends('backoffice.main')

@section('content')

<div class="main-content">
    <div class="col-md-12">
        <section class="box">
            <div class="box-body">
            <div id="tabs">

                    <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\ReviewsController@update') }}" id="formExemplo" data-toggle="validator" role="form">
                        {{ csrf_field() }}
                                    <p class="title-form"> Edit Review  </p>
                                    <div class="form-group">	    
                                        <label for="textTitle" class="control-label">{{ __('backoffice/form.name') }}</label>	    
                                        <input name="name" id="textTitle" class="form-control" value="{{ $reviews->name }}" data-error="{{ __('backoffice/form.name-error') }}" type="text" required>	  
                                        <div class="help-block with-errors">
                                        </div>
                                    </div>	 

                                    <div class="form-group">	    
                                        <label for="textTitle" class="control-label">{{ __('backoffice/form.company') }}</label>	    
                                        <input name="company" id="textTitle" class="form-control" value="{{ $reviews->company }}" data-error="{{ __('backoffice/form.company-error') }}" type="text" required>	  
                                        <div class="help-block with-errors">
                                        </div>
                                    </div>	  	

                                    <div class="form-group">	    
                                        <label for="textTitle" class="control-label">{{ __('backoffice/form.quote') }}</label>	    
                                        <input name="quote" id="textTitle" class="form-control" value="{{ $reviews->quote }}" data-error="{{ __('backoffice/form.quote-error') }}" type="text" required>	  
                                        <div class="help-block with-errors">
                                        </div>
                                    </div>	

                                    <div class="form-group">	    
                                        <label for="thumbnail" class="control-label">{{ __('backoffice/form.image') }}</label>	    
                                        <input name="thumbnail[]" id="thumbnail" class="form-control" placeholder="{{ __('backoffice/form.image') }}" type="file" data-error="{{ __('backoffice/form.image-error') }}" accept="image/*" >	    
                                        <?php $path = '/images/reviews/'. $reviews->id.'/'.$reviews->thumbnail; ?>
                                        <img class="news-thumb" src="<?php echo $path; ?>">
                                        <div class="help-block with-errors">
                                        </div>	  
                                    </div>  
                                    <input type="hidden" name="id" value="{{ $reviews->id }}"> 
                                    <div class="form-group submit-form">
                                        <button type="submit" id="submit" class="btn btn-primary">{{  __("backoffice/form.submit-button") }}</button>
                                    </div>		 
                    </form>
            </div>
        </section>
        
    </div>
    </div>
</div>

@endsection
