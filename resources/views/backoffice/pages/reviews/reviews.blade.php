@extends('backoffice.main')

@section('content')


<div class="main-content">
    <div class="col-md-12">

        <section class="content-header">
            <h1> {{ __('backoffice/global.reviews') }} </h1>
        </section>





        <a href="{{ route('admin.reviews.create') }}"> <button type="button" class="btn btn-success" >{{ __('backoffice/reviews.add') }}</button> </a>

        <section class="box">
            <div class="box-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('backoffice/reviews.name') }}</th>
                            <th scope="col">{{ __('backoffice/reviews.company') }}</th>
                            <th scope="col">{{ __('backoffice/reviews.quote') }}</th>
                            <th scope="col">{{ __('backoffice/reviews.image') }}</th>
                            <th scope="col">{{ __('backoffice/reviews.actions') }}</th>
                        </tr>
                    </thead>
                    @foreach ($reviews as $review)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $review->id }}</th>
                                <td>{{ $review->name }}</td>
                                <td>{{ $review->company }}</td>
                                <td> <?php echo substr($review->quote, 0,35).'...'; ?> </td>
                                <td>
                                    <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $review->id }}">{{ __('backoffice/reviews.preview') }}</button>
                                        
                                        <div class="modal fade" id="{{ $review->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                            <p><q> {{ $review->quote }}</q></p>
                                                            <p> - {{ $review->name }}, {{$review->company}} </p>
                                                            <img src={{ asset('/images/reviews/' . $review->id . '/'. $review->thumbnail) }}>
                                                    </div>
                                                </div>
                                                </div>
                                        </div>               
                                </td>      
                                <td>
                                    <a href="{{ route('admin.reviews.edit', ['id' => $review->id ])}}"> <i class="fa fa-edit icon-edit"></i> </a> 
                                    <a href="{{ route('admin.reviews.delete', ['id' => $review->id ])}}" onclick="return confirm('{{ __('backoffice/form.reviews-alert-remove') }}')"> <i class="fa fa-trash-alt icon-trash-alt"></i> </a> 
                                </td>
                            </tr>
                        </tbody>
                    @endforeach
                        <tfoot>
                            <tr class="thead-dark">
                                <th scope="col">#</th>
                                <th scope="col">{{ __('backoffice/reviews.name') }}</th>
                                <th scope="col">{{ __('backoffice/reviews.company') }}</th>
                                <th scope="col">{{ __('backoffice/reviews.quote') }}</th>
                                <th scope="col">{{ __('backoffice/reviews.image') }}</th>
                                <th scope="col">{{ __('backoffice/reviews.actions') }}</th>
                            </tr>
                        </tfoot>
                </table>

            </div>
        </section>
        
    </div>
    </div>

    


</div>
{!! Toastr::message() !!}


@endsection