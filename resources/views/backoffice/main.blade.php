@include('backoffice.partials._head')
@include('backoffice.partials._main-header')
@include('backoffice.partials._sidebar')

<body>
    <div class="container-fluid">
        @yield('content')
    </div>
</body>
<html>
