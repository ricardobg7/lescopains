
    <?php 
        $message->from($email, $name);
        $message->to(['r.guerreiro@comup.pt']);       
        $message->subject('Contact Form');
    ?>
    
    <h1> Contact Form </h1>
    <div class="main-body">
        <strong> Nom: </strong> {{ $name }} <br>      
        <strong> Email: </strong> {{ $email }} <br>


        @if(!empty($description))
            <strong> Message: </strong> {{ $description }} <br>
        @endif
    </div>


  