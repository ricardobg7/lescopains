
    <?php 
        $message->from($email, $firstname);
        $message->to(['r.guerreiro@comup.pt']);       
        $message->subject('Adhesion Form');
    ?>
    
    <h1> Adhesion Form </h1>
    <div class="main-body">

        <strong> Gender: </strong> {{ $gender }} <br>
        <strong> Nom: </strong> {{ $firstname }} <br>      
        <strong> Prenom: </strong> {{ $lastname }} <br>
        <strong> Address: </strong> {{ $address }} <br>
        <strong> Secondary Address : </strong> {{ $secondaryAddress }} <br>
        <strong> Ville: </strong> {{ $ville }} <br>
        <strong> Postal Code: </strong> {{ $postalCode }} <br>
        <strong> Telephone: </strong> {{ $phone }} <br>
        <strong> Adhesion: </strong> {{ $adhesion }} <br>
    </div>
