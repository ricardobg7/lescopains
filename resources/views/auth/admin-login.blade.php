@extends('layouts.app')

@section('content')

    <div class="form">
        <div class="thumbnail">
            <img src="http://lesdauphins.fr/img/dauphins-logo-1.png"/>
        </div>
        <form class="login-form" method="POST" action="{{ route('admin.login') }}">
            {{ csrf_field() }}
        
            <input id="email" placeholder="email@email.com" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            <input id="password" placeholder="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            <button type="submit" class="btn btn-primary">login</button>
            <a class="btn btn-link" href="{{ route('admin.password.request') }}">
                <p>Forgot Your Password?</p>
            </a>
        </form>
    </div>
@endsection
