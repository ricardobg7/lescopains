<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'menu' => 'MENU',
    'dashboard' => 'Dashboard',
    'dashboard-description' => 'Google Analytics & Info',
    'news' => 'News',
    'policies' => 'Privacy Policies',
    'reviews' => 'Reviews',
    'contacts' => 'Contacts',
    'users' => 'Users',
    'newsletter' => 'Newsletter',
    'logout' => 'Logout',
    'logged' => 'Logged in as:',
    'edit-profile' => 'Edit Profile',
];
