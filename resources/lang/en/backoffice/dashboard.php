<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'users' => 'Users',
    'views' => 'Total views',
    'time' => 'Time',
    'most-viewed' => 'Most viewed pages',
    'most-used-device' => "Most used devices"

];
