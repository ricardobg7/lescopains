<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'title' => 'Title',
    'title-error' => 'Please fill the title',
    'subtitle' => 'Sub title',
    'subtitle-error' => 'Please fill the sub title',
    'description' => 'Description',
    'description-error' => 'Please fill the description',
    'name' => 'Name',
    'name-error' => 'Please fill the name',
    'company' => 'Company',
    'company-error' => 'Please fill the company',
    'quote' => 'Quote',
    'quote-error' => 'Please fill the quote',
    'image' => 'Image',
    'image-error' => 'Please choose one image',
    'state' => 'State',
    'state-publish' => 'Publish',
    'state-unpublish' => 'Unpublish',
    'state-error' => 'Please choose one option',
    'next-button' => 'Next',
    'submit-button' => 'Submit',
    'news-add-success' => 'News added with success',
    'news-remove-success' => 'News removed with success',
    'news-alert-remove' => 'Are you sure? News are going to be deleted permanently.',
    'news-alert-duplicate' => 'Are you sure? News are going to be duplicated.',
    'news-duplicate-success' => 'News duplicated with success',
    'news-updated-success' => 'News updated with success',
    'policies-updated-success' => 'Policies updated with success',
    'policies-remove-success' => 'Policies removed with success',
    'user-add-success' => 'User added with success',
    'user-updated-success' => 'User updated with success',
    'user-exists' => 'User Already Exists',
    'user-remove-sucess' => 'User removed with success',
    'contacts-update-sucess' => 'Contacts updated with sucess',
    'contacts-remove-sucess' => 'Contacts removed with sucess',
    'reviews-add-sucess' => 'Review added with success',
    'reviews-update-sucess' => 'Review updated with sucess',
    'reviews-remove-sucess' => 'Review removed with sucess',
    'reviews-alert-remove' => 'Are you sure? Review will be deleted permanently.',
    'reviews-alert-duplicate' => 'Are you sure? Review will be duplicated.',
    'newsletter-remove-sucess' => 'Newsletter email removed with sucess',







];