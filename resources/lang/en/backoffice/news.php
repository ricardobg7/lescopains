<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'id' => 'ID',
    'title' => 'Title',
    'subtitle' => 'Sub title',
    'description' => 'Description',
    'thumbnail' => 'Thumbnail',
    'actions' => 'Actions',
    'actions-edit' => 'Edit',
    'actions-remove' => 'Remove',
    'actions-add' => 'Add News',

];
