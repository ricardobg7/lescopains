<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'edit' => 'Edit Contacts',
    'email' => 'Email',
    'phone' => 'Phone',
    'secondaryphone' => 'Secondary Phone',
    'adress' => 'Adress',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'linkedin' => 'Linkedin',
];
