<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'users' => 'Utilisateurs',
    'views' => 'Vues totales',
    'time' => 'Temps',
    'most-viewed' => 'Pages les plus vues',
    'most-used-device' => "Périphériques les plus utilisés"

];
