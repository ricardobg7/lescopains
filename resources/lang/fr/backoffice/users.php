<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'firstname' => 'Prénom',
    'lastname' => 'Nom de famille',
    'email' => 'Email',
    'type' => 'Type',
    'actions' => 'Actions',
    'password' => 'Mot de Passe',
    'add' => 'Ajouter un nouvel utilisateur',

];
