<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'menu' => 'MENU',
    'dashboard' => 'Tableau de bord',
    'dashboard-description' => 'Google Analytics & Info',
    'news' => 'Nouvelles',
    'policies' => 'Politiques de confidentialité  ',
    'faqs' => 'FAQS',
    'faqs-description' => 'Questions fréquemment posées',
    'contacts' => 'Contacts',
    'users' => 'Utilisateurs',
    'newsletter' => 'Newsletter',
    'logout' => 'Se Déconnecter',
    'logged' => 'Connecté en tant que:',
    'edit-profile' => 'Modifier le profil',

];