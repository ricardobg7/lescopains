<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'id' => 'ID',
    'title' => 'Titre',
    'subtitle' => 'Sous-titre',
    'description' => 'Description',
    'thumbnail' => 'La Vignette',
    'actions' => 'Actes',
    'actions-edit' => 'Modifier',
    'actions-remove' => 'Retirer',
    'actions-add' => 'Ajouter Nouvelles',

];
