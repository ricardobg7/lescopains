<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'title' => 'Titre',
    'title-error' => 'S\'il vous plaît remplir le titre',
    'subtitle' => 'Sous-titre',
    'subtitle-error' => 'Veuillez remplir le sous-titre',
    'description' => 'La Description',
    'description-error' => 'S\'il vous plaît remplir la description',
    'image' => 'Image',
    'image-error' => 'Veuillez choisir une image',
    'state' => 'Etat',
    'state-publish' => 'Publier',
    'state-unpublish' => 'Non publié',
    'state-error' => 'Veuillez choisir une option',
    'next-button' => 'Prochain',
    'submit-button' => 'Soumettre',
    'news-add-success' => 'Nouvelles ajoutées avec succès',
    'news-remove-success' => 'Nouvelles supprimées avec succès',
    'news-alert-remove' => 'Êtes-vous sûr? Les nouvelles vont être définitivement supprimées.',
    'news-alert-duplicate' => 'Êtes-vous sûr? Les nouvelles vont être dupliquées.',
    'news-duplicate-success' => 'Nouvelles dupliquées avec succès',
    'news-updated-success' => 'Nouvelles mises à jour avec succès',
    'policies-updated-success' => 'Policie édité avec succès',
    'policies-remove-success' => 'Règles supprimées avec succès',
    'user-add-success' => 'Utilisateur ajouté avec succès',
    'user-updated-success' => 'Utilisateur mis à jour avec succès',
    'user-exists' => 'L\'utilisateur existe déjà',
    'user-remove-success' => 'Utilisateur supprimé avec succès',
    'contacts-update-success' => 'Contacts mis à jour avec succès',
    'contacts-remove-success' => 'Contacts supprimés avec succès',
    'faqs-add-success' => 'Faqs ajouté avec succès',
    'faqs-update-success' => 'Faqs mis à jour avec succès',
    'faqs-remove-success' => 'Faqs enlevés avec succès',
    'newsletter-remove-success' => 'E-mail de newsletter supprimé avec succès',





];
