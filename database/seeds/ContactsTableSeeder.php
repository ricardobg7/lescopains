<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
	        DB::table('contacts')->insert([
	            'email' => "contact@lescopainsdhugo.org",
                
                'adress' => "Maison des Associations 14, Place du Clos de Pacy 94370 Sucy-en-Brie",

                'facebook' => "https://www.facebook.com/lescopainsdhugo",

                'created_at' => date("Y-m-d H:i:s")
	        ]);
    }
}