[33mcommit ad208b617ea57964d03268d62df8262f2a3ebeb5[m
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 13:20:54 2018 +0100

    fix erros on controllers/models

[33mcommit 844eee0e4b1f61de12d4f42e1c653ae6b3eecc47[m
Merge: d5ebce7 64cca67
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 12:00:59 2018 +0100

    Merge branch 'master' of https://bitbucket.org/comupagency/backoffice

[33mcommit 64cca6714194fead94b4b631dab995ef08f5889a[m
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Tue Apr 17 12:00:26 2018 +0100

    Fix migrations/seeds || Create Models

[33mcommit d5ebce75f24a234b6041ab5f538ca19b6018f74c[m
Merge: ef00828 fd6ede2
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 11:12:48 2018 +0100

    Merge branch 'master' of https://bitbucket.org/comupagency/backoffice

[33mcommit fd6ede2fe27e9dd3e28b3b0405ef0cf4c85a66b0[m
Merge: 093c5ab 7782fb9
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Tue Apr 17 11:12:28 2018 +0100

    Merge branch 'languages'

[33mcommit ef00828bfcf3b28c5ca581aa8f6a7b948a22ca1b[m
Merge: 7782fb9 093c5ab
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 11:10:27 2018 +0100

    Merge branch 'master' of https://bitbucket.org/comupagency/backoffice

[33mcommit 093c5abd48cd0b07e484f43840347fca22104282[m
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Tue Apr 17 11:03:42 2018 +0100

    Create seeds for all tables

[33mcommit 7782fb9ba86c4a7f154b24b94a9a599ab3955556[m
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 10:59:58 2018 +0100

    add models to control data

[33mcommit 9378629b32960985649bd3be4b0dfdd23f8282d9[m
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 09:40:16 2018 +0100

    add global language files

[33mcommit 83de96cc02a62a3641e3d1f0b0ac19cb3d35ca44[m
Author: unknown <f.fernandes@comup.pt>
Date:   Tue Apr 17 08:57:16 2018 +0100

    add new languages and routes

[33mcommit 99051af86fca803aef6cae38af1d5a1ce930c6a9[m
Author: rguerreiro <r.guerreiro@comup.pt>
Date:   Tue Apr 17 01:39:54 2018 +0100

    Created seeders for News and Users

[33mcommit 84d31bd0e6fca1e3620c27ae277b6c71412a567e[m
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Mon Apr 16 16:30:48 2018 +0100

    Fixed folder structure

[33mcommit b1ba77f57721052b8631f65b92eb515a4211146b[m
Author: unknown <f.fernandes@comup.pt>
Date:   Mon Apr 16 16:05:09 2018 +0100

    added folders to language

[33mcommit ecd9d9aa4f7f315550534139f125b80ff6f42b9b[m
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Mon Apr 16 13:14:43 2018 +0100

    fixed news translation table

[33mcommit 8c9ae26203e38ed7a9f102e6a6106ee0de5e5f65[m
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Mon Apr 16 13:08:52 2018 +0100

    Create migrations

[33mcommit 61e5d60c3f8cff5b5d03e94d116169d847adc845[m
Author: unknown <f.fernandes@comup.pt>
Date:   Mon Apr 16 11:47:42 2018 +0100

    add files to folders

[33mcommit 4c2835c7faecb09c9bc0750e32b7a1dc87af9a8e[m
Author: unknown <f.fernandes@comup.pt>
Date:   Mon Apr 16 11:44:59 2018 +0100

    add page to frontend

[33mcommit 0b7dd51f10ce36338272407ece78fe83fc1e724a[m
Author: rguerreiro <ricardoguerreiro94@gmail.com>
Date:   Mon Apr 16 11:41:51 2018 +0100

    tst

[33mcommit 2d1ddbe41941d7041397a18cf98445fea678c181[m
Author: unknown <f.fernandes@comup.pt>
Date:   Mon Apr 16 10:42:16 2018 +0100

    create project
