# BACKOFFICE 

## About

Project developed with the Laravel framework, with the main goal being designing a simple and functional backoffice, yet constantly growing and evolving, that can be adapted for each client.
This project is divided in packages: Basic and Advanced.

The Basic package has a set of pre determined features and functionalities:

- Dashboard, multiple google analytics graphs/data 
- News, table with all news data (title, sub title, description, thumbnail). Can create news, edit/delete/duplicate existing ones
- Privacy Policies,table with all PP data (title, description). Can preview current PP and edit it. Limit of one PP.
- FAQS, table with all FAQS data (Quesntion & Answer). Can create/edit/delete FAQS
- Users, table with all users (Admins and users), containing all data (First Name, Last Name, Email, Type). Can create, edit and delete.
- Contacts, table with all Contacts data (Email, phone, Secondary Phone, Address, Social Media). Only can be edited.
- Newsletter, all names & emails subscribed to the newsletter. Can be downloaded as a CSV file.
	
The Advanced package contains all the basic package features and any other missing functionalities the client asks or we deem required for the project.

## Installation

### Requirements

For development we use [Laragon](https://laragon.org/download/).

The project requires the following frameworks and technologies to run/work on:

- [PHP 7.0+](http://php.net/downloads.php).
- [MySQL]().
- [Laravel 5.4](https://laravel.com/docs/5.4).
- [Git](https://git-scm.com/downloads).
- [Composer](https://getcomposer.org/).
- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/get-npm).


## Usage

Always check [Laravel's documentation]() in case of any doubt, since it has most it's features well documented and explained.

We use [VSCode](https://code.visualstudio.com/), so here's a small list of plugins we recommend to help you coding:

- [Indent-Rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)
- [Laravel Blade Snippets](https://marketplace.visualstudio.com/items?itemName=onecentlin.laravel-blade)
- [Rainbow Brackets](https://marketplace.visualstudio.com/items?itemName=2gua.rainbow-brackets)
- [phpcs](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs)

All code is divided in two subfolders throughout the project, Frontoffice and Backoffice.

This repository is only meant for the main Backoffice code, being the Frontoffice part of the code developed in each respective repository (with any required changes to it's backoffice counterpart)


After cloning the repo to your computer, make sure to verify if your ".env" file is setup is correctly.


The file ".env.example" will either contain the info you need to use, or dummy data to exemplify what you need to do/write.



The project will contain two folders: migrations and seeds.

The migrations folder will have all the tables files, with all necessary foreign keys, data types and so on.

The seed folder will have all the data to populate the respective tables with dummy data (or actual data if the repo already contains production data)
		
The following command will create all tables and seed them accordingly:


		php artisan migrate:refresh --seed 
		
To create a new table use:

		php artisan make:migration create_tableName_table
		
To create a seed for the new table use:

		php artisan make:seeder TableNameTableSeeder
		
After creating a new seeder, make sure to add it to the DatabaseSeeder.php file, so it's called whenever seeding the database.


The project will require some basic structure to run any new pages, which can be quickly seen [here](https://laravel.com/docs/5.1/quickstart). [^1]

We'll quickly gloss over that basic structure and the requirements to create a new page before you start developing!


**Routes, Views and Controllers**

#### Routes

You can find all of the project's route in the following file:

		root/routes/web.php
		
In this file you will declare any new routes, for example:
	
		Route::get('/', 'frontend\HomeController@index')->name('home');

In the example above we declare..

*The url:*
	
			" Route::get('/' "
				
*The controller:*
				
			" 'frontend\HomeController@index' "
	
*The name of our route:*
	
			" ->name('home') "


#### Views



#### Controllers




## Extras

### Debugging Tools

- Clean composer's cache and artisan's cache & config

		composer clearcache && php artisan cache:clear && php artisan config:clear
		
- Clear browser cache or try opening in an anon page

- run npm to make sure all js and css/sass has been compiled properly
	
		npm run dev
		
		

### SSH Keys
#### Locally
- Create a pair of keys with the command:

		ssh-keygen
	
#### Server
- Copy the id_rsa.pub key to your server with the following commando, which will prompt you to insert your password:

		 ssh-copy-id username@hostname

#### Bitbucket
- [Check on the official BitBucket docs](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html).


### Git Bash Alias

- Go to root with: 
        
        cd ~
        
- Create file .bash_profile:
        
        touch .bash_profile
        
- Add desired alias, ex:

        alias home='cd ~'
        
### Git Commands


List of useful commands for git:

- Check list of branches (local and remote)

        git branch -a
		
- Checkout to branch:

		git checkout [branch name]
		
- Add changes:

        git add . && git add -u && git add -A
		
- Commit changes:

        git commit -m "Insert message here"

- Push changes to remote/origin

        git push origin {branch name]
		
- Reset all changes locally to origin state

        git reset --hard origin/[branch name]
		
- Check all head states / list last changes made locally

		git reflog
		
- Check all commits
		
		git log


## Credits

This project was developed by [Fabio Fernandes](https://www.linkedin.com/in/fabiomfernandes/) and [Ricardo Guerreiro](https://www.linkedin.com/in/ricardo-guerreiro-406814141/), for the company Comup Agenc with all rights reserved.


[^1]: Even tho the quickstart documentation is for Laravel 5.1, most of its contents still are pertinent to the current version.
