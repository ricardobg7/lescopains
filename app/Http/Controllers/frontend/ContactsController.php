<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contacts;
use Auth;
use App\Mail\ContactsMail;
use Illuminate\Support\Facades\Mail;
use Toastr; 


class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        
    }

    
    public function index()
    {
        $contacts = DB::table('contacts')->get();

        return view('frontend.pages.contacts.contacts', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contactus(Request $request)
    {
        
        $email = $_POST['email'];
        $name = $_POST['name'];
        $description = $_POST['message'];


        Mail::send('emails.contacts', 
            [   'email' => $email, 
                'name' => $name, 
                'description' => $description
            ], 
            function ($message){

        });

        Toastr::success('Email envoyé avec succès', 'Formulaire de contact', [   "positionClass" => "toast-top-center"]);

        return Redirect::back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adhesion(Request $request)
    {
        $gender = $_POST['inlineRadioOptions'];
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $address = $_POST['address'];
        $secondaryAddress = $_POST['secondaryAddress'];
        $ville = $_POST['ville'];
        $postalCode = $_POST['postalCode'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $adhesion = $_POST['adhesion'];

        Mail::send('emails.adhesion', 
            [   'email' => $email, 
                'gender' => $gender,
                'firstname' => $firstname, 
                'lastname' => $lastname,
                'address' => $address, 
                'secondaryAddress' => $secondaryAddress, 
                'ville' => $ville, 
                'postalCode' => $postalCode, 
                'phone' => $phone,
                'adhesion' => $adhesion, 
            ], 
            function ($message){

        });

        Toastr::success('Email envoyé avec succès', "Bulletin d'adhesion", [   "positionClass" => "toast-top-center"]);

        return Redirect::back();  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
