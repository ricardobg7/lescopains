<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Newsletter;
use Auth;
use Toastr; 



class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {

    }


    
    public function index()
    {
        $contacts = DB::table('contacts')->get();
        $news = DB::table('newstranslations')->orderBy('created_at', 'desc')->take(4)->get(); 
        $reviews = DB::table('reviews')->orderBy('created_at', 'desc')->get(); 
        return view('frontend.pages.home', compact('contacts', 'news', 'reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $newsletter = new Newsletter();
        $newsletter->email =  $_POST['email'];
        
        if(Newsletter::where('email', '=', $newsletter->email )->count() > 0){
            Toastr::error('Déjà inscrit dans la newsletter', 'Inscription à la Newsletter', [   "positionClass" => "toast-top-center"]);

        }else{
            $newsletter->save();
            Mail::send('emails.newsletter', 
                [   
                    'email' => $newsletter->email, 
                ], function ($message){
            });
            
            Toastr::success('Abonné avec succès à notre newsletter', 'Inscription à la Newsletter', [   "positionClass" => "toast-top-center"]);
        }

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


}
