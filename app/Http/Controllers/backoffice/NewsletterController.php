<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

use App\Http\Controllers\Controller;
use App\Newsletter;
use Auth;
use Response;
use Toastr; 



class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    
    public function index()
    {
        $user = Auth::user();
        $lang = $this->getCurrentLanguage();
        $newsletters = DB::table('newsletter')->get();
        return view('backoffice.pages.newsletter.newsletter', compact('newsletters','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Remove from newsletter 
        $languages = LaravelLocalization::getSupportedLocales();
        $newsletter = new Newsletter;

        //remove from newsletter
        $newsletter = newsletter::destroy($id); 


        Toastr::success('Newsletter was deleted with success', 'Newsletter', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/newsletter');

    }

    public function download()
    {
        $time = time();
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
        ,   'Content-type'        => 'text/csv'
        ,   'Content-Disposition' => 'attachment; filename=Newsletter_subscribers'.date("Ymd",$time).'.csv'
        ,   'Expires'             => '0'
        ,   'Pragma'              => 'public'
        ];

        $list = Newsletter::all('email')->toArray();
        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list) 
            {
                $FH = fopen('php://output', 'w');
                foreach ($list as $row) { 
                    fputcsv($FH, $row);
                }
                fclose($FH);
            };

        return Response::stream($callback, 200, $headers);
    }

    /** 
     * Get current language
     */
    public function getCurrentLanguage(){
        $currentLang = App::getLocale();
        $lang = DB::table('languages')->where('name', $currentLang)->first();
        return $lang->id;
    }
}
