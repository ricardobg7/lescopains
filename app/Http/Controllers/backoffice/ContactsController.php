<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

use App\Http\Controllers\Controller;
use App\Contacts;
use Auth;
use Toastr; 

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    
    public function index()
    {
        $user = Auth::user();
        $lang = $this->getCurrentLanguage();
        $contacts = DB::table('contacts')->get();
        return view('backoffice.pages.contacts.contacts', compact('contacts','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contacts = DB::table('contacts')->get();
        return view('backoffice.pages.contacts.edit-contacts', compact('contacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

        $updatedContacts = array('email' => $_POST['email'],'phone' => $_POST['phone'],'secondaryPhone' => $_POST['secondaryPhone'],
        'adress' => $_POST['adress'],'facebook' => $_POST['facebook'],'twitter' => $_POST['twitter'],'linkedin' => $_POST['linkedin']);

        DB::table('contacts')->where('id', $_POST['id'])
            ->update($updatedContacts);

        Toastr::success('Contacts was edited with success', 'Contacts', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/contacts'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Remove from news translations 
        $languages = LaravelLocalization::getSupportedLocales();
        $policiesTranslations = new PoliciesTranslations;
        $policiesTranslations->delete();
            
        return Redirect::to('admin/politica')->with('message', __('backoffice/form.contacts-remove-success'));
     
    }

    /** 
     * Get current language
     */
    public function getCurrentLanguage(){
        $currentLang = App::getLocale();
        $lang = DB::table('languages')->where('name', $currentLang)->first();
        return $lang->id;
    }
}
