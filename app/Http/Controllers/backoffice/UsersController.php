<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Illuminate\Http\Request;
use App\Admin;
use App\User;
use Illuminate\Validation\Validator;
use Toastr; 

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    
    public function index()
    {
        $admins = DB::table('admins')->orderBy('type')->get();
        $users = DB::table('users')->get();
        $currentUser = Auth::user();
        $allUsers = array();
 
        if(!empty($admins)){
            foreach($admins as $admin){
                array_push($allUsers, $admin);
            }
        }

        if(!empty($user)){

            foreach($users as $user){
                array_push($allUsers, $user);
            } 
        }
        

        return view('backoffice.pages.users.users', compact('allUsers','currentUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.pages.users.add-users');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // CREATE USER
        $admin = new Admin(); 
        $user = new User();

        //RUN ALL LANGUAGES AND SAVE TO DATABASE
        //ADMIN
        $admin->firstName =  $request->request->get('firstName');
        $admin->lastName =  $request->request->get('lastName');
        $admin->email =  $request->request->get('email');
        $admin->password =  bcrypt($request->request->get('lastName'));
        $admin->type =  $request->request->get('type');
        //USER
        $user->firstName =  $request->request->get('firstName');
        $user->lastName =  $request->request->get('lastName');
        $user->email =  $request->request->get('email');
        $user->password =  bcrypt($request->request->get('lastName'));
        $user->type =  $request->request->get('type');

        if($admin->type == '4'){
            if(User::where('email', '=', $admin->email)->count() > 0 || Admin::where('email', '=', $admin->email)->count() > 0){
                return Redirect::to('admin/users/create')->with('message', __('backoffice/form.user-exists'));
            }else{
                $user->save();
            }
        }else{
            if(Admin::where('email', '=', $admin->email)->count() > 0 || User::where('email', '=', $admin->email)->count() > 0){
                return Redirect::to('admin/users/create')->with('message', __('backoffice/form.user-exists'));
            }else{
                $admin->save();
            }
        }

        Toastr::success('User was created with success', 'Users', [   "positionClass" => "toast-top-center"]);
        
        return Redirect::to('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$userType)
    {
        $currentUser = Auth::user();
        if($userType == 4){
            $user = DB::table('users')->where('id', $id)->get();
        }else{
            $user = DB::table('admins')->where('id', $id)->get();
        }
        return view('backoffice.pages.users.edit-users', compact('user','currentUser')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $updatedUser = array('firstName' => $_POST['firstName'], 'lastName' => $_POST['lastName'],'email' => $_POST['email'],'password' => bcrypt($_POST['password']),
        'type' => $_POST['type']);

        if($_POST['oldType'] == 4){
            DB::table('users')->where('id', $_POST['id'])
            ->update($updatedUser);
        }else{
            DB::table('admins')->where('id', $_POST['id'])
            ->update($updatedUser);
        }

        Toastr::success('User was edited with success', 'Users', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/users');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$userType)
    {
            
        if($userType == '4'){
            $user = User::destroy($id); 
        }else{
            $admin = Admin::destroy($id); 
        }
        //remove from admin
        Toastr::success('User was deleted with success', 'Users', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/users');  
    }
}
