<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Reviews;
use Auth;
use Toastr; 


class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    public function index()
    {
        $user = Auth::user();
        $lang = $this->getCurrentLanguage();
        $reviews = DB::table('reviews')->get();
        return view('backoffice.pages.reviews.reviews', compact('reviews','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();
        return view('backoffice.pages.reviews.add-reviews', ['langs' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //GET DATA FROM REQUEST
        $data['name'] =  $request->request->get('name');
        $data['company'] =  $request->request->get('company');
        $data['quote'] =  $request->request->get('quote');
        $data['thumbnail'] = str_replace(" ", '',$_FILES['thumbnail']['name'][0]); 

        // CREATE REVIEW
        $reviews = Reviews::create($data);

        //image
        $path = public_path().'/images/reviews/'.$reviews->id;
    
        if(!File::exists($path)) {
            File::makeDirectory($path,0777,true);

        }
        

        Input::file('thumbnail')[0]->move($path, str_replace(" ", '', $_FILES['thumbnail']['name'][0]));


        //SAVE TO DB NEW REVIEW
        $reviews->save();

        Toastr::success('Review was created with success', 'Reviews', [   "positionClass" => "toast-top-center"]);
        return Redirect::to('admin/reviews');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reviews = DB::table('reviews')->where('id', '=', $id)->first();
        $langs = LaravelLocalization::getSupportedLocales();
        
        return view('backoffice.pages.reviews.edit-reviews', compact('langs','reviews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {   
        $updatedReview = array('name' => $_POST['name'], 'company' => $_POST['company'], 'quote' => $_POST['quote']);
        
        //get folder + files
        $dir = public_path().'/images/reviews/'.$_POST['id']; 

        $files = preg_grep('/^([^.])/', scandir($dir));

        //get new image
 
        $images = $_FILES['thumbnail']['name'];

        //replace old image for new image
        if(isset($files[2])){
            if($images[0] != $files[2]){
                if($images[0] != ""){
                    unlink($dir.'/'.$files[2]); 
                    Input::file('thumbnail')[0]->move( $dir , str_replace(" ", '', $_FILES['thumbnail']['name'][0]));
                }
            }
        }

        $imgs = array();
        array_push($imgs, preg_grep('/^([^.])/', scandir($dir)) );

        $path = public_path().'/images/reviews/'.$_POST['id'];


        DB::table('reviews')->where('id', $_POST['id'])
        ->update($updatedReview);

        Toastr::success('Review was edited with success', 'Reviews', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/reviews');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

            
        //remove from faqs
        $reviews = Reviews::destroy($id); 

        Toastr::success('Review was deleted with success', 'Reviews', [   "positionClass" => "toast-top-center"]);
        
        return Redirect::to('admin/reviews');
    }
        /** 
     * Get current language
     */
    public function getCurrentLanguage(){
        $currentLang = App::getLocale();
        $lang = DB::table('languages')->where('name', $currentLang)->first();
        return $lang->id;
    }
}
