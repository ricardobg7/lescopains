<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\News;
use App\NewsTranslations;
use Auth;
use Toastr; 




class NewsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $user = Auth::user();
        $lang = $this->getCurrentLanguage();
        $news = DB::table('newstranslations')->get();
        return view('backoffice.pages.news.news', compact('news','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $languages = LaravelLocalization::getSupportedLocales();
        return view('backoffice.pages.news.add-news');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // CREATE NEWS
        $news = News::create($request->request->all()); 

        //RUN ALL LANGUAGES AND SAVE TO DATABASE
        $languages = LaravelLocalization::getSupportedLocales();


        $path = public_path().'/images/news/'.$news->id;
    
        if(!File::exists($path)) {
            File::makeDirectory($path,0777,true);

        }


 
            $newsTranslations = new newsTranslations;
            $newsTranslations->title =  $request->request->get('textTitle');
            $newsTranslations->subtitle =  $request->request->get('textSubtitle');
            $newsTranslations->description =  $request->request->get('textDescription');
            $newsTranslations->thumbnail =  $_FILES['thumbnail']['name'][0];
            $newsTranslations->newsId =  $news->id;
            $newsTranslations->thumbnail = str_replace(" ", '', $newsTranslations->thumbnail);


            Input::file('thumbnail')[0]->move($path, str_replace(" ", '', $_FILES['thumbnail']['name'][0]));
        

            $newsTranslations->save();

        

        //Create a path folder to store images per news
        //$path =  url('resources/assets/images/Backoffice'. $news->id);


        Toastr::success('News was created with success', 'News', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/news');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $newsTranslations = DB::table('newstranslations')->where('newsId', '=', $id)->get();
        $newsTranslations = $newsTranslations[0]; 
        $news = DB::table('news')->where('id', '=', $id)->get();

        $langs = LaravelLocalization::getSupportedLocales();
        return view('backoffice.pages.news.edit-news', compact('langs','news','newsTranslations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  
     * @return \Illuminate\Http\Response
     */
    public function update()
    {            
        //Get current files in folder
        $dir = public_path().'/images/news/'.$_POST['id']; 


        
        $files = preg_grep('/^([^.])/', scandir($dir));
        
        //Get uploaded files 
        $images = $_FILES['thumbnail']['name'];

        if(isset($files[2])){
            if($images[0] != $files[2]){
                if($images[0] != ""){
                    unlink($dir.'/'.$files[2]); 
                    Input::file('thumbnail')[0]->move( $dir , str_replace(" ", '', $_FILES['thumbnail']['name'][0]));
                }
            }
        }

        

        $imgs = array();
        array_push($imgs, preg_grep('/^([^.])/', scandir($dir)) );

        $langs = LaravelLocalization::getSupportedLocales();
        $path = public_path().'/images/news/'.$_POST['id'];

        $z = 0;
            $updatedNews = array('title' => $_POST['textTitle'],'subtitle' => $_POST['textSubtitle'],'description' => $_POST['textDescription'],'thumbnail' => $imgs[0][2]);

            DB::table('newstranslations')->where('newsId', $_POST['id'])
                                        ->update($updatedNews);

            $z++;

        Toastr::success('News was edited with success', 'News', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/news');  

    }

/**
     * Duolicate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {  

        
        $newsTranslations = new newsTranslations;
        $newsTranslations = DB::table('newstranslations')->where('newsId', '=', $id)->get();

        $news = new News(); 
        $newOld = $news->where('id', '=', $id)->get();
        $newNews = News::create($newOld[0]['attributes']); 
        



            $oldPath = public_path().'/images/news/'.$id;
            $path = public_path().'/images/news/'.$newNews['attributes']['id'];
     
            $dir = opendir($oldPath); 
            File::makeDirectory($path,0777,true);

            while(false !== ( $file = readdir($dir)) ) { 
                if (( $file != '.' ) && ( $file != '..' )) { 
                    if ( is_dir($oldPath . '/'. $file) ) { 
                        recurse_copy($oldPath . '/'. $file,
                                        $path . '/'. $file); 
                    } 
                    else { 
                        
                        copy($oldPath .'/'.$file  ,
                            $path. '/'.$file ); 
                    } 
                } 
            } 
   





        foreach ($newsTranslations as $key => $news){
            $newsTranslations = new newsTranslations;

            $newsTranslations->title =  $news->title;
            $newsTranslations->subtitle =  $news->subtitle;
            $newsTranslations->description =  $news->description;
            $newsTranslations->thumbnail =  $news->thumbnail;

            $newsTranslations->newsId =  $newNews['attributes']['id'];

            $newsTranslations->save();
        }

        Toastr::success('News was duplicated with success', 'News', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/news');   
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
        $path = public_path().'/images/news/'.$id;

        $success = File::deleteDirectory($path);

        //Remove from news translations 
        $languages = LaravelLocalization::getSupportedLocales();
        $newsTranslations = new newsTranslations;
        $newsTranslations->where('newsId', $id)->delete();
            
        //remove from news
        $news = News::destroy($id); 

        Toastr::success('News was deleted with success', 'News', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/news');

    }

    /** 
     * Get current language
     */
    public function getCurrentLanguage(){
        $currentLang = App::getLocale();
        $lang = DB::table('languages')->where('name', $currentLang)->first();
        return $lang->id;
    }

    public function getAllLanguages(){
        $lang = DB::table('languages')->get();
        return $lang;
    }
}
