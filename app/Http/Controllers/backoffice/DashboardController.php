<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use Analytics;
use Spatie\Analytics\Period;
use Lava;


class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data =  Analytics::fetchTopBrowsers(Period::days(7));
        $pages = Analytics::fetchMostVisitedPages(Period::days(7));
        $topReferer = Analytics::fetchTopReferrers(Period::days(7)); 
        $users = Analytics::performQuery(Period::days(30), 'ga:users');
        $users = $users->rows[0][0];
 
        $views = Analytics::fetchMostVisitedPages(Period::days(30), 1);
        $views = $views[0]['pageViews'];
        $time = Analytics::performQuery(Period::days(30), 'ga:sessionDuration');
        $time = $time[0][0];
        $time = $time / 60;
        $time = number_format((float)$time, 2, '.', '').'h';

        //returns devices + browsers 
        $devices = app('App\Services\Devices')->devices();
        $devicesUsed = app('App\Services\Devices')->devicesUsed($devices);
        //returns most viewed pages
        $trending = app('App\Services\Trending')->week();
        //returns regions (only country atm)
        $countries = app('App\Services\Regions')->regions();

        // Generates PieChart with devices used data
        $reasons = \Lava::DataTable();
        $reasons->addStringColumn('Devices')
                ->addNumberColumn(1);

            foreach($devices as $device){
                $reasons ->addRow( [ $device['device'], (int)$device['pageviews'] ]);
            }

        \Lava::PieChart('devicesPie', $reasons, [
            'is3D'   => false,
            'chartArea' =>[
                'width'=>'75%',
                'height'=>'70%'
                ]

        ]);

        
        // Generates GeoChart with Country data
        $map = \Lava::DataTable();
        $map->addStringColumn('Country')
                ->addNumberColumn('Views');

        foreach($countries as $country){
            $map ->addRow( [ $country[0], (int)$country[1] ]);
        }
        \Lava::GeoChart('map', $map, [

            'colorAxis' => [
                'colors' => ['#449eff', '005abb']],
            'height' => 500,
            'sizeAxis' => [
                'minValue' => 1 ],
            'legend' => 'none'   
        ]);

    
        return view('backoffice.pages.dashboard.dashboard', compact('trending', 'devices', 'devicesPie', 'map', 'users', 'views', 'time' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
