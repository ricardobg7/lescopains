<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    protected $fillable = [ 'name', 'company','quote', 'thumbnail'];

    protected $table = 'reviews';
}
